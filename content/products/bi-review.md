+++
title = "BI Review"
duration = "4 hours"
service = "Business Intelligence"
+++
Review tooling and processes, evaluate quick-wins and best practices to implement
