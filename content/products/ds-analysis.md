+++
title = "DS analysis"
duration = "20 hours"
service = "Data Science"
+++
Exploratory analysis towards validation of a business hypothesis.
