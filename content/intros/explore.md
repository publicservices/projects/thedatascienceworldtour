+++
title = "Explore"
image = "/media/uploads/explore.jpg"
+++
Each established company, start-up or simple project represents for me an opportunity to discover a new cultural environment for applying data discovery methods. I am a curious and enthusiastic data nerd, who can't wait to play around with some new data set and discover how entreprises can take the most out of it around the world!
