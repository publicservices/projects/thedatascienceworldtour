+++
title = "Create"
image = "/media/uploads/create.jpg"
+++
I graduated from the Data Science Master program at the EISTI Grande Ecole in France in 2015. With this beautiful toolbox of theoretical knowledge, I dedicate my work to bring powerful algorithm into empowering data solutions to businesses. The innovational aspect of my work lies into bringing an appropriate proof-of-concept or solution to a specific business challenge. Not the other way around!
