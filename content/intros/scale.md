+++
title = "Scale"
image = "/media/uploads/scale.jpg"
+++
My experience in the Berlin startup scene allowed me to follow the creation and scaling of data operations teams: from tracking to business intelligence towards data science. That methodological and squared mind of mine helps those structures define roles and build a scalable ground for meaningful data exploration projects as well as data products.
