+++
title = "Contact"
menu = "main"
layout = "contact"
+++
Please reach out if you have a data story to share

* [github](https://github.com/SixtineVervial)
* [linkedin](https://www.linkedin.com/in/sixtine-vervial-40222580/)

\
\
\
\
\
\
\
\
\
\
\

\----------------\
Sixtine Vervial\
\
Freelance Data Scientist registered in France\
Micro-entreprise sous régime de l'URSSAF\
MLE VERVIAL SIXTINE MARIE V CONSEIL AUX ENTREPRISES ­ SIRET 80897424000018 ­ Profession Libérale

All pictures taken from real travel stories - subject to copyright

\----------------

Kudos to <https://hugovieilledent.gitlab.io/> for providing a solid framework for this website.
