+++
title = "Personalisation, now?"
date_published = "2019-10-30"
+++
A couple of days ago, I was invited to speak in LeROI's lovely office* about the key role of data specialists in making each user-journey special. We explored quick data-hacks anyone with a little database could implement for data science in action!

![slide1](../../media/uploads/IMG_20190709_173022.jpg)

Over the past years, my work has been focused on applying mathematical concepts from all data science techniques to business problems. For instance, getting to know your customers could be represented by a clustering analysis over their various forms of engagement with your product. A persona study results of finding groups in your weighted user graph, understanding what interest they share, and design adapted messages, variations of communications and dedicated features.

![slide2](../../media/uploads/create.jpg)

\* [Snowplow Analytics](https://snowplowanalytics.com/webinars/data-modeling-mini-series/?utm_source=referral&utm_medium=sixtinevervial&utm_campaign=freeforfriends) : brilliant tracking tool and so much more