+++
title = "Career - from maths to production"
date_published = "2019-03-15"
+++
Some weeks ago, as I was casually working from the (way too cool) rooftop terrace of The Hive coworking space in Bangkok*, my friend Chris rings in and offers to have me on his podcast The Floor Is Lava. Both terrified and excited about doing my first public talk in German, I accept.

The [podcast](https://open.spotify.com/episode/54RklXO1BMwAu297ZaSzBn) went out (in German), and for those who still struggle with the beautiful language of Goethe, I will sum up the few key points for you here.

Thank you @Chris for such an interesting chat, looking forward to further cooperation!

\* The Hive coworking spaces - best hosting team ever

<hr/>

#### How did I become a Freelance Data Scientist?

By accident? Following my interest for mathematics, I picked an intensive ("classe préparatoire") scientific formation after high-school. That lead me to choose an engineering school for my master's degree which offered applied mathematics. Among all specialties, decision support systems, now called Data Science, caught my attention. Later on, various experiences working with startups helped me build operational knowledge.

#### What can data experts do?

Generally, I would define data science as the art of manipulating data in a programmatic way, to enable enlightened business decisions. Data analysis would refer to the activity of discovering trends within a certain dataset (snapshot), whereas machine learning includes the idea of autonomous evolution of those trends, and automated update. Data engineers help us collect and clean the data for enabling further research. Data product managers design deployment strategy and product integration. 

#### How do they interact with businesses?

Overall, data workers are technical experts, who offer tools for specific industrial / services development. They are no magicians and the data never "speaks for itself". If I don't know your product, I will apply the standard acquisition > conversion > retention strategy. But if you sell properties, retention is probably out of relevance! 

Another common misconception is that having many dashboard and metrics is key. What really is key is to define success criteria for your business, then give the data teams the ressources to track relevant user engagement steps and compute KPIs for you. Similarly, building prediction models is interesting, but only relevant if you can measure a return on investment (KPI uplift / development costs). Therefore, I advice to always start with the definition of the challenge before jumping into algorithm specifics.
