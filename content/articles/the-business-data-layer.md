+++
title = "The business data layer"
+++
Why bother maintaining dozens of ETLs, customer trackers, datetime and currency conversions and other data's darkest secrets? I asked myself, every Q/A day. 

Why argue about naming conventions, office IP filters and annoying product categories? I wonder every cross-team meeting.

Because having a couple of clean and accurate tables, with well thought definitions is key for enabling our product managers and VPs make data-driven decisions and creating quality growth. When a sales representative can quickly check on a market size on a way to a client meeting, when a product manager starts a daily meeting with all A/B tests results, the company moves fast and safer.

![](media/uploads/Meetup LeRoi personalisation.jpg)

For each challenge, a couple of indicators clearly defined will enable to follow the impact of each deployment on the product's success. Then, please allow your BI team the resources to build robust pipes, that will avoid heavy maintenance and migration costs in the future - the earlier you start, the sooner the juicy numbers.

![](media/uploads/Meetup LeRoi personalisation (6).jpg)

The ideal starting point for any data analysis (user engagement, marketing campaign, feature usage) or data product (recommendation engine, targeted retention) is precisely this set of couple of clean and accurate tables, with well thought definitions we were chatting about.

![](media/uploads/Meetup LeRoi personalisation (7).jpg)



\-> In [this article](./whitepaper-a-universal-data-model-for-building-personalised-applications), we explore a universal data model for building personalised applications.
