+++
title = "Hello world!"
date_published = "1955-01-01"
+++

Summer 2018, second summer at JustWatch, the business-intelligence team is up and running, allowing to fully switch off from work during the holidays. Thanks to interesting but smooth running projects with my employer, I had managed to dedicate time to side-projects during the year and grew in a network with various ambitions. Starting to see a recurring pattern in demands, I thought maybe there would be enough data to crunch for me to do it full time.

Those first missions helped me see how cultures influence the shape of data teams and structures in different industries, cities, civilizations, even political contexts. Usages as well, and the different ways of supporting a business with it. My old passion for abstraction didn’t need more. “How do diverse cultures practice data driven management around the world?”. The question got stuck in my head as a wild meditation topic for the winter coming.

On January 9th 2019, I sat in a plane heading to Singapore with a fresh excitement for what could happen next. No real plan in mind, except for filling up a new domain thedatascienceworldtour.com and shaping my next professional step. Or as Clem would say : grow with the flow.

This series of posts reflect my own, personal and totally subjective opinion. Last before jumping into learning, a huge THANK YOU to all friends, colleagues and teams who accepted this unconventional way of working and spoiled me with interesting challenges. I’m ready for more.


This website is a work in progress.

A first contentful version will be release in spring 2019. 

Spoiler alert: it will talk about data & stuff.

Stay tuned!

