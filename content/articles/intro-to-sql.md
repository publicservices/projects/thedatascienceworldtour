+++
title = "Intro to SQL"
date_published = "2018-11-01"
+++

Anyone blogging about data has his very own SQL tutorial. I built my first best version with a smart colleague of mine back in Berlin in 2015, which was revisited early this year with a cool french startup.

### Tutorial

Link to [Intro to SQL](https://docs.google.com/presentation/d/1yLcIchUMBZfoukaCMXbZxK0ZN6dYzsXWAGeCbnGpfOA/edit?usp=sharing) presentation

### Readme

#### Who is this training made for?
Anyone curious about retrieving data on his/her own. No prior knowledge about databases or programmation is needed.

#### What will you learn?
The very basics of database environments, how to write simple SQL queries and execute them to extract data from a data warehouse.

#### What will you skip?
This training does not cover database management, advanced SQL functions nor visualisation techniques.
