+++
title = "#1 business intelligence"
draft = false
intro = "Business intelligence (BI) comprises the strategies and technologies used by enterprises for the data analysis of business information. BI technologies provide historical, current, and predictive views of your business operations. "
+++

# Towards sustainable architectures
- Inventory data sources, outputs and daily needs
- Review tooling and processes for data transformations
- Assess permissions management
- Refine data roles and hiring priorities

# From raw to business data
- Standardise dimensions and metrics definition
- Build clean "business data layer"
- Train non-tech users on usage
