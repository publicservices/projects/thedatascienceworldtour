+++
title = "#4 data products"
draft = false
intro = "Your marketing or operations team has a specific challenge that could be solved with data? Let’s look at what data science could do for you."
+++
