+++
title = "#3 data systems audit"
draft = false
intro = "Let’s sit down for a minute and look at your data resources to build together a scalable plan in order to squeeze the best value out of your tracking."
+++
GDPR compliance

Analytical stack review
- tracking / data sources
- ETLs
- data warehouse
- reporting