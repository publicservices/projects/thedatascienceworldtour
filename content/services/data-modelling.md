+++
title = "#2 data modelling"
draft = false
intro = "Define KPIs and simplify access to your data by building a \\“ready-to-query\\” data layer for your analysts. Deep-dive analysis two lines of SQL away!"
+++
