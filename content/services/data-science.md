+++
title = "#5 data science"
intro = "Use scientific methods, processes, algorithms and systems to extract knowledge and insights from your data."
+++

# Data-driven business development
- Define hypothesis to test
- Deep-dive into datasets and trend analysis
- Design usage strategy and product integration

# Data science project management
- Define success criteria
- Data cleaning strategy
- Technical support (refactoring, API design)
- Design user feedback loop
- Deployment strategy (A/B testing)
