+++
title = "#6 busines process mining"
draft = false
intro = "Gather data from various systems interacting during a user journey, a parcel delivery or a payroll - and empower operation managers with conformance checking, KPI monitoring."
+++
