export default {
    label: 'Tags',
    name: 'tags',
    required: false,
    widget: 'array',
    hint: 'The layout, is which visual template will be used on this page. It defines the appearence and functionnality of the pageon the website.',
    options: ['', 'no-title']
}
