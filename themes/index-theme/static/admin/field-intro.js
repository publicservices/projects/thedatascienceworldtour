export default {
    required: false,
    label: 'Intro',
    name: 'intro',
    widget: 'string',
    hint: 'The intro text, visible on the card.'
}
