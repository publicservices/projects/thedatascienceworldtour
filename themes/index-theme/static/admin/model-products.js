import title from './field-title.js'
import slug from './field-slug.js'
import draft from './field-draft.js'
import body from './field-body.js'
import duration from './field-duration.js'

const products  = {
    name: 'products',
    label: 'Products',
    label_singular: 'Product',
    folder: 'content/products',
    format: 'toml-frontmatter',
    create: true,
    slug: '{{title}}',
    editor: {
	preview: false
    },
    fields: [
	title,
	slug,
	draft,
	duration,
	body
    ]
}

export default products
