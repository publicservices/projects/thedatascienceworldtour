import products from './model-products.js'
import intros from './model-intros.js'
import services from './model-services.js'
import articles from './model-articles.js'
import pages from './model-pages.js'
import configs from './model-configs.js'

export default [
    intros,
    products,
    services,
    articles,
    pages,
    configs
]
