import title from './field-title.js'
import slug from './field-slug.js'
import draft from './field-draft.js'
import body from './field-body.js'
import featuredImage from './field-featured-image.js'

const intros  = {
    name: 'intros',
    label: 'Intros',
    label_singular: 'Intro',
    folder: 'content/intros',
    format: 'toml-frontmatter',
    create: true,
    slug: '{{title}}',
    editor: {
	preview: false
    },
    fields: [
	title,
	slug,
	draft,
	featuredImage,
	body
    ]
}

export default intros
