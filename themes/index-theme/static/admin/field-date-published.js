export default {
    label: 'Published date',
    name: 'date_published',
    widget: 'date',
    require: true,
    default: '',
    format: 'YYYY-MM-DD',
    hint: 'A publication date for this item.'
}
