export default {
    label: 'Duration',
    name: 'duration',
    widget: 'string',
    hint: 'The duration for this product, ex: "40 hours"'
}
