import collections from './collections.js'

export default {
    config: {
	// Skips config.yml.
	// By not skipping, the configs will be merged, with the js-version taking priority.
	load_config_file: false,
	display_url: window.location.origin,

	backend: {
	    name: 'gitlab',
	    branch: 'master',
	    repo: 'sixtinevervial/thedatascienceworldtour',
	    auth_type: 'implicit',
	    app_id: '2b445d588af1ac90f5df52e197f0a41c46308cb19e99abdbf2fa49a2597f92d2'
	},

	media_folder: 'static/media/uploads',
	public_folder: '/media/uploads',

	collections
    }
}
