import title from './field-title.js'
import slug from './field-slug.js'
import draft from './field-draft.js'
import body from './field-body.js'
import intro from './field-intro.js'
import featuredImage from './field-featured-image.js'

const services = {
    name: 'services',
    label: 'Services',
    label_singular: 'Service',
    create: true,
    format: 'toml-frontmatter',
    folder: 'content/services',
    editor: {
	preview: false
    },
    fields: [
	title,
	slug,
	draft,
	intro,
	featuredImage,
	body
    ]
}

export default services
