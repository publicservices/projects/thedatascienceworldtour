import title from './field-title.js'
import slug from './field-slug.js'
import draft from './field-draft.js'
import body from './field-body.js'
import datePublished from './field-date-published.js'

const articles  = {
    name: 'articles',
    label: 'Articles',
    label_singular: 'Article',
    folder: 'content/articles',
    format: 'toml-frontmatter',
    create: true,
    slug: '{{title}}',
    editor: {
	preview: false
    },
    fields: [
	title,
	slug,
	draft,
	datePublished,
	body
    ]
}

export default articles
